from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.views.generic import ListView,CreateView,UpdateView,DetailView,DeleteView
from tarea.models import *
from tarea.forms import *

# Create your views here.
class DeleteMuerte(DeleteView):
    model=Muertes
    template_name='eliminar.html'
    def get_success_url(self):
        return reverse('depurar')
    
class DetailMuertes(DetailView):
    model = Muertes
    template_name="detalle.html"
    
class ListMuertesView(ListView):
    model=Muertes
    template_name="depurar.html"
    
class CreateMuertesView(CreateView):
    model=Muertes
    template_name='nuevo.html'
    form_class=MuertesForm

    def get_context_data(self, **kwargs):
        context=super(CreateMuertesView,self).get_context_data(**kwargs)
        context['action']=reverse('nuevo')
        return context

    def get_success_url(self):
        return reverse('nuevo')

class UpdateMuertesView(UpdateView):
    model=Muertes
    template_name='nuevo.html'
    form_class=MuertesForm

    def get_context_data(self, **kwargs):
        context=super(UpdateMuertesView,self).get_context_data(**kwargs)
        context['action']=reverse('nuevo',kwargs={'pk':self.get_object().idMuerte})
        return context

    def get_success_url(self):
            return reverse('nuevo')

def inicio(request):
    return render(request, "index.html",{'muerte':Muertes.objects.all(),'fechas':Fecha.objects.all()})

#def depurar(request):{{form.as_p}}
#    return render(request, "depurar.html",{'muerte':Muertes.objects.all()})

def login(request):
    return render(request, "login.html", {})

def primera(request):
    return render(request, "principal.html", {'muerte':Muertes.objects.all()})

#def nuevo(request):
#    return render(request, "nuevo.html", {})
