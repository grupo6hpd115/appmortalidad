from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse

# Create your models here.
class Persona(models.Model):
    idPersona=models.CharField(max_length=5,primary_key=True)
    nombre=models.CharField(max_length=50,null=False)
    apellido=models.CharField(max_length=50,null=False)
    genero=models.CharField(max_length=10,null=False)
    correo=models.CharField(max_length=30,null=False)
    edad=models.IntegerField()
    fechaNacimiento=models.DateField()
    direccion=models.CharField(max_length=200,null=False)

    def __str__(self):
        return self.nombre

class Administrador(models.Model):
    idAdministrador=models.CharField(max_length=5,primary_key=True)
    persona=models.ForeignKey(Persona,null=False)
    contrasena=models.CharField(max_length=15)

    def __str__(self):
        return self.persona.nombre

class Fecha(models.Model):
    idFecha=models.CharField(max_length=5,primary_key=True)
    anio=models.CharField(max_length=15,null=False)
    mes=models.CharField(max_length=15,null=False)

    def __str__(self):
        return self.mes+" / "+self.anio

class Causas(models.Model):
    idCausas=models.CharField(max_length=5,primary_key=True)
    nombreCausas=models.CharField(max_length=100,null=False)

    def __str__(self):
        return self.nombreCausas

class Grafico(models.Model):
    idGrafico=models.CharField(max_length=5,primary_key=True)
    tipoGrafico=models.CharField(max_length=50,null=False)

    def __str__(self):
        return self.tipoGrafico

class Muertes(models.Model):
    idMuerte=models.AutoField(max_length=5,primary_key=True)
    causas=models.ForeignKey(Causas,null=False)
    fecha=models.ForeignKey(Fecha,null=False)
    cantidadHombres=models.IntegerField()
    cantidadMujeres=models.IntegerField()
    total=models.IntegerField()

    def get_absolute_url(self):
        return reverse('detalle',kwargs={'pk':self.idMuerte})
    
    def __str__(self):
        return self.causas.nombreCausas

class InformeMortalidad(models.Model):
    idInforme=models.CharField(max_length=5,primary_key=True)
    persona=models.ForeignKey(Persona,null=False)
    administrador=models.ForeignKey(Administrador,null=False)
    grafico=models.ForeignKey(Grafico,null=False)
    muerte=models.ForeignKey(Muertes,null=False)
    tipoArchivo=models.CharField(max_length=5,null=False)

    def __str__(self):
        return self.idInforme
