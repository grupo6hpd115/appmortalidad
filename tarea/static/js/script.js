     $(document).ready(main);

     var contador = 1;

     function main () {
     	$('.menu_bar').click(function(){
     		if (contador == 1) {
     			$('nav').animate({
     				left: '0'
     			});
     			contador = 0;
     		} else {
     			contador = 1;
     			$('nav').animate({
     				left: '-100%'
     			});
     		}
     	});

     	// Mostramos y ocultamos submenus
     	$('.submenu').click(function(){
     		$(this).children('.children').slideToggle();
     	});
      //inicio de secion
      $(document).ready(function(){
   $('.log-btn').click(function(){
       $('.log-status').addClass('wrong-entry');
      $('.alert').fadeIn(500);
      setTimeout( "$('.alert').fadeOut(1500);",3000 );
   });
   $('.form-control').keypress(function(){
       $('.log-status').removeClass('wrong-entry');
     });

   });
 }
 //Validar campos solo numero
 function justNumbers(e)
      {
      var keynum = window.event ? window.event.keyCode : e.which;
      if ((keynum == 8) || (keynum == 46))
      return true;

      return /\d/.test(String.fromCharCode(keynum));
      }

 //generar grafico
 $(function () {
    $('#container').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '3D chart with null values'
        },
        subtitle: {
            text: 'Notice the difference between a 0 value and a null point'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        xAxis: {
            categories: Highcharts.getOptions().lang.shortMonths
        },
        yAxis: {
            title: {
                text: null
            }
        },
        series: [{
            name: 'Sales',
            data: [2, 3, null, 4, 0, 5, 1, 4, 6, 3]
        }]
    });
});
