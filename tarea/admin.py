from django.contrib import admin
from tarea.models import Persona,Administrador,Fecha,Causas,Grafico,Muertes,InformeMortalidad
# Register your models here.

class PersonaAdmin(admin.ModelAdmin):
    list_display = ('nombre','apellido','genero','correo','edad','fechaNacimiento','direccion')

class AdministradoresAdmin(admin.ModelAdmin):
    list_display = ('persona','contrasena')

class FechaAdmin(admin.ModelAdmin):
    list_display = ('anio','mes')

class CausasAdmin(admin.ModelAdmin):
    list_display = ('idCausas','nombreCausas')

class GraficoAdmin(admin.ModelAdmin):
    list_display = ('idGrafico','tipoGrafico')

class MuertesAdmin(admin.ModelAdmin):
    list_display = ('idMuerte','causas','fecha','cantidadHombres','cantidadMujeres','total')

class InformeMortalidadAdmin(admin.ModelAdmin):
    list_display = ('administrador','grafico','muerte')

admin.site.register(Persona,PersonaAdmin)
admin.site.register(Administrador,AdministradoresAdmin)
admin.site.register(Fecha,FechaAdmin)
admin.site.register(Causas,CausasAdmin)
admin.site.register(Grafico,GraficoAdmin)
admin.site.register(Muertes,MuertesAdmin)
admin.site.register(InformeMortalidad,InformeMortalidadAdmin)
