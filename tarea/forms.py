from django import forms
from django.core.exceptions import ValidationError
from tarea.models import *

class MuertesForm(forms.ModelForm):

    cantidadHombres=forms.IntegerField(required=True,label="Cantidad Hombres")
    cantidadMujeres=forms.IntegerField(required=True,label="Cantidad Mujeres")
    total=forms.IntegerField(required=True,label="Total")

    class Meta:
        model = Muertes
        fields= '__all__'

    def clean_cantidadHombres(self):
        cantidadHombres=self.cleaned_data.get('cantidadHombres')

        if cantidadHombres<0:
            raise forms.ValidationError("Debe ser mayor a cero")

        return cantidadHombres

    def clean_cantidadMujeres(self):
        cantidadMujeres=self.cleaned_data.get('cantidadMujeres')

        if cantidadMujeres<0:
            raise forms.ValidationError("Debe ser mayor a cero")

        return cantidadMujeres

    def clean_total(self):
        total=self.cleaned_data.get('total')

        if total<0:
            raise forms.ValidationError("Debe ser mayor a cero")

        return total
