"""proyecto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from tarea.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', 'tarea.views.primera', name='primera'),
    url(r'^iniciar_secion$', 'tarea.views.login', name='login'),
    url(r'^(?P<pk>\d+)/$',DetailMuertes.as_view(),name='detalle'),
    url(r'^eliminar-(?P<pk>\d+)/$',DeleteMuerte.as_view(),name='eliminar'),
    url(r'^actualizar-(?P<pk>\d+)/$',UpdateMuertesView.as_view(),name='nuevo'),
    url(r'^depurar$',ListMuertesView.as_view(), name='depurar'),
    url(r'^nuevo$',CreateMuertesView.as_view(), name='nuevo'),
    url(r'^inicio$', 'tarea.views.inicio', name='inicio'),


]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
